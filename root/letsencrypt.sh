#!/usr/bin/env bash

/usr/bin/certbot --nginx certonly --agree-tos --renew-by-default --email REPLACEME@REPLACEME.TLD -d REPLACEME.TLD,www.REPLACEME.tld,mail.REPLACEME.tld
