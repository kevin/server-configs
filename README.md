# Base Config

## Notes:

1. The Mail Server requires SSL certificates
2. Occurrences of `REPLACEME.TLD` without comments stating otherwise should have the FQDN substituted for it
3. Occurrences of `REPLACEME` without comments stating otherwise should have the FQDN without the TLD substituted for it

## Swap File

1. Run `dd if=/dev/zero of=/swapfile bs=1M count=2048` then `chmod 600 /swapfile` followed by `mkswap /swapfile` and `swapon /swapfile`
2. Set the system up to mount the swap file at boot by adding `/swapfile none swap defaults 0 0` to the bottom of `/etc/fstab`

## Web Server

### Package Requirements for Web Server

nginx php php-apcu-bc php-fpm php-composer php-gd php-imap php-intl php-memcached php-geoip geoip-database geoip-database-extra memcached mariadb npm certbot certbot-nginx

### Folders for Web Server

nginx php systemd

### Setup Instructions for Web Server

1. Install the packages in the `Package Requirements` above
2. Copy the folders above that aren't already configured to their equivalent location in `/etc`
3. In `/etc/nginx/sites-available/REPLACEME.TLD.conf` and `/root/letsencrypt.sh`
4. Rename `/etc/nginx/sites-available/REPLACEME.TLD.conf`
5. Add your site files to `/srv/http/REPLACEME.TLD` where public assets are located in `/srv/http/REPLACEME.TLD/public`
6. Create a symlink from `/etc/nginx/sites-available/REPLACEME.tld.conf` to `/etc/nginx/sites-enabled/REPLACEME.tld.conf`
7. Run `openssl dhparam -out /etc/nginx/dhparam.pem 4096` to generate the diffie-hellman parameter
8. Run `systemctl start php-fpm nginx` to start the web services and `systemctl status php-fpm` and `systemctl status nginx` to check for errors
9. If there were no errors in the previous command, run `systemctl enable php-fpm nginx` to enable the web services at boot
10. Ensure the public web directory exists, update the list of domains in `/root/letsencrypt.sh` and then run it to generate the SSL certificates
11. Run `systemctl start certbot-renewal.timer` and `systemctl enable certbot-renewal.timer` to start and enable the auto-renewal process

### MySQL Config

1. Run `mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql`
2. Run `systemctl start mysqld` and `systemctl enable mysqld` to start mysqld and enable it at boot
3. Run `mysql_secure_installation`.
4. Create a new password.
5. Say `yes` to everything (e.g. "Remove anonymous users? Disallow root logging remotely? Remove test database and access to it? Reload privilege tables now?")

## Mail Server

### Package Requirements for Mail Server

dovecot postfix procmail opendkim amavisd-new spamassassin unrar p7zip

### Folders for Mail Server

amavisd dovecot pam.d postfix procmailrc skel systemd opendkim

### Setup Instructions for Mail Server

1. Install the packages in the `Package Requirements` above
2. Copy the folders above that aren't already configured to their equivalent location in `/etc`
3. Add an A DNS record for the FQDN and a hostname for the server
4. Set the hostname of the server with `hostnamectl set-hostname SOMETHING.REPLACEME.TLD` where `SOMETHING` is a unique name for the server and `REPLACEME.TLD` is the domain. This will be the server's new hostname.
5. Add the hostname of the server to the end of the line starting with `127.0.0.1` in `/etc/hosts`
6. In `/etc/amavisd/amavisd.conf`, `/etc/dovecot/dovecot.conf` and `/etc/opendkim/opendkim.conf` replace occurrences of `REPLACEME.TLD` with the domain, and occurrences of `REPLACEME` with the first part of the domain
7. In `/etc/postfix/main.cf` replace `SOMETHING.REPLACEME.TLD` with the hostname of the server and `REPLACEME.TLD` with the domain
8. Run `opendkim-genkey -r -s REPLACEME -d REPLACEME.TLD` where `REPLACEME.TLD` is the domain, and `REPLACEME` is the first part of the domain
9. In `/etc/postfix/aliases`, replace the instance of `REPLACEME` with the user that should receive domain-level emails
10. Run `openssl dhparam -out /etc/dovecot/dh.pem 4096`
11. Run `newaliases` to update the aliases database with the contents of `/etc/postfix/aliases`
12. Run `sa-update` to update spamassassin
13. Run `systemctl start amavisd postfix dovecot opendkim` to start the mail services and `systemctl status amavisd postfix dovecot opendkim` to check for errors
14. If there were no errors in the previous command, run `systemctl enable amavisd postfix dovecot opendkim` to enable the mail services at boot
15. Create an MX DNS record for `REPLACEME.TLD` containing the hostname (`REPLACEME.TLD` can usually be left out of the input field)
16. Create a TXT DNS record for the host `REPLACEME._domainkey.REPLACEME.TLD` containing `v=DKIM1; k=rsa; s=email; p=PASSWORD`, replacing the occurrence of `REPLACEME` with the first part of the domain, `REPLACEME.TLD` with the full domain, and `PASSWORD` with the string following `p=` in `/etc/opendkim/REPLACEME.txt` (`REPLACEME.TLD` can usually be left out of the input field)
17. Create a TXT DNS record for the host `REPLACEME.TLD` containing `v=spf1 mx -all` (`REPLACEME.TLD` can usually be left out of the input field)
18. Create a TXT DNS record for the host `_dmarc.REPLACEME.TLD` containing `v=DMARC1; p=none` (`REPLACEME.TLD` can usually be left out of the input field)
19. Set the reverse DNS record for the VPS to the hostname
20. Add `postfix.service` and `dovecot.service` to the `ExecStartPost` service reload in `/etc/systemd/system/certbot-renewal.service` and run `systemctl daemon-reload`

### Add Accounts

1. Create an account by running `useradd -m -d /home/REPLACEME -s /bin/bash REPLACEME` (replacing `REPLACEME` with the username associated with the mail account)
2. Set the password by running `passwd REPLACEME` (replacing `REPLACEME` with the username associated with the mail account)
